@file:Suppress("ConstantConditionIf")

package com.blacklyon.dynamicscheduler.injection

import com.blacklyon.dynamicscheduler.domain.session.SessionRepository
import com.blacklyon.dynamicscheduler.domain.turn.Hour
import com.blacklyon.dynamicscheduler.domain.turn.TurnRepository
import com.blacklyon.dynamicscheduler.domain.user.UserRepository
import com.blacklyon.dynamicscheduler.infrastructure.repositories.inmemory.InMemorySessionRepository
import com.blacklyon.dynamicscheduler.infrastructure.repositories.inmemory.InMemoryTurnRepository
import com.blacklyon.dynamicscheduler.infrastructure.repositories.inmemory.InMemoryUserRepository
import com.blacklyon.dynamicscheduler.infrastructure.repositories.remote.RemoteSessionRepository
import com.blacklyon.dynamicscheduler.infrastructure.repositories.remote.RemoteTurnRepository
import com.blacklyon.dynamicscheduler.infrastructure.repositories.remote.RemoteUserRepository
import com.blacklyon.dynamicscheduler.infrastructure.retrofit.DateSerializer
import com.blacklyon.dynamicscheduler.infrastructure.retrofit.IDoctorAPI
import com.blacklyon.dynamicscheduler.infrastructure.retrofit.IPatientAPI
import com.blacklyon.dynamicscheduler.infrastructure.retrofit.IServiceAPI
import com.blacklyon.dynamicscheduler.view.doctor.main.DoctorViewModel
import com.blacklyon.dynamicscheduler.view.doctor.session.SessionViewModel
import com.blacklyon.dynamicscheduler.view.login.LoginViewModel
import com.blacklyon.dynamicscheduler.view.patient.advance.AdvanceTurnViewModel
import com.blacklyon.dynamicscheduler.view.patient.main.MenuViewModel
import com.blacklyon.dynamicscheduler.view.patient.turn.NextTurnViewModel
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*


object Injection {
    private const val useRemote = true
    var actualTime: Hour = Hour(Calendar.getInstance())

    fun loginViewModel() : LoginViewModel = LoginViewModel(userRepository = userRepository)

    fun nextTurnViewModel() : NextTurnViewModel =
        NextTurnViewModel(userRepository = userRepository, repository = turnRepository)

    fun menuViewModel() : MenuViewModel =
        MenuViewModel(userRepository = userRepository, turnRepository = turnRepository)

    fun advanceTurnViewModel() : AdvanceTurnViewModel =
        AdvanceTurnViewModel(userRepository = userRepository, turnRepository = turnRepository)


    fun doctorViewModel(): DoctorViewModel =
        DoctorViewModel(userRepository = userRepository, sessionRepository = sessionRepository)

    fun sessionViewModel(): SessionViewModel = SessionViewModel(sessionRepository = sessionRepository)

    private val userRepository: UserRepository by lazy {
        if (useRemote) RemoteUserRepository(serviceApi)
        else InMemoryUserRepository()
    }

    private val turnRepository: TurnRepository by lazy {
        if (useRemote) RemoteTurnRepository(patientApi)
        else InMemoryTurnRepository()
    }

    private val sessionRepository: SessionRepository by lazy {
        if (useRemote) RemoteSessionRepository(doctorApi)
        else InMemorySessionRepository()
    }

    private val retrofitBuilder: Retrofit by lazy { createRetrofit() }

    private val serviceApi: IServiceAPI by lazy { createServiceApi(retrofitBuilder) }
    private val patientApi: IPatientAPI by lazy { createPatientApi(retrofitBuilder) }
    private val doctorApi: IDoctorAPI by lazy { createDoctorApi(retrofitBuilder) }

    private fun createRetrofit(): Retrofit {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val client = OkHttpClient.Builder().addInterceptor(interceptor).build()

        return Retrofit.Builder()
            .client(client)
            .addConverterFactory(GsonConverterFactory.create(createGsonInstance()))
            .baseUrl("http:192.168.0.100:4200/")
            .build()
    }

    private fun createGsonInstance(): Gson {
        return GsonBuilder()
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
            .registerTypeAdapter(Date::class.java, DateSerializer())
            .create()
    }
    private fun createServiceApi(retrofit: Retrofit): IServiceAPI {
        return retrofit.create(IServiceAPI::class.java)
    }

    private fun createPatientApi(retrofit: Retrofit): IPatientAPI {
        return retrofit.create(IPatientAPI::class.java)
    }

    private fun createDoctorApi(retrofit: Retrofit): IDoctorAPI {
        return retrofit.create(IDoctorAPI::class.java)
    }
}