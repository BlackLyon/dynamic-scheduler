package com.blacklyon.dynamicscheduler.infrastructure.repositories.inmemory

import com.blacklyon.dynamicscheduler.domain.Result
import com.blacklyon.dynamicscheduler.domain.session.Session
import com.blacklyon.dynamicscheduler.domain.session.SessionRepository
import com.blacklyon.dynamicscheduler.domain.turn.Hour
import com.blacklyon.dynamicscheduler.domain.turn.Turn
import java.util.*

class InMemorySessionRepository: SessionRepository {
    private var id = 0

    override suspend fun acceptTurn(doctorId: Int): Result<Session> {
        val session = Session(++id, Hour(1, 10), Hour(2, 5), "Paciente")
        return Result.Success(session)
    }

    override suspend fun extendSession(sessionId: Int): Result<Hour> {
        return Result.Success(Hour(11, 30))
    }

    override suspend fun endSession(sessionId: Int): Result<Boolean> {
        return Result.Success(true)
    }

    override suspend fun acceptEmergency(doctorId: Int): Result<Session> {
        val session = Session(++id, Hour(1, 10), Hour(2, 5), "Paciente")
        return Result.Success(session)
    }
}