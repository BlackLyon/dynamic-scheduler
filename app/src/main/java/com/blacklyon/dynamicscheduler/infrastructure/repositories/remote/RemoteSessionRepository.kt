package com.blacklyon.dynamicscheduler.infrastructure.repositories.remote

import com.blacklyon.dynamicscheduler.domain.Result
import com.blacklyon.dynamicscheduler.domain.session.Session
import com.blacklyon.dynamicscheduler.domain.session.SessionRepository
import com.blacklyon.dynamicscheduler.domain.turn.Hour
import com.blacklyon.dynamicscheduler.domain.turn.Turn
import com.blacklyon.dynamicscheduler.infrastructure.dto.RequestDto
import com.blacklyon.dynamicscheduler.infrastructure.retrofit.IDoctorAPI
import com.blacklyon.dynamicscheduler.injection.Injection
import kotlinx.coroutines.CoroutineStart
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.withContext
import kotlin.coroutines.CoroutineContext

class RemoteSessionRepository(
    private val remote: IDoctorAPI,
    private val coroutineContext: CoroutineContext = Dispatchers.IO
): SessionRepository {

    override suspend fun acceptTurn(doctorId: Int): Result<Session> {
        return apiRequest("No posee turno que aceptar") { remote.acceptNextTurn(RequestDto(doctorId, Injection.actualTime)) }
    }

    override suspend fun acceptEmergency(doctorId: Int): Result<Session> {
        return apiRequest("No se ha encontrado turno") { remote.acceptEmergency(RequestDto(doctorId, Injection.actualTime)) }
    }

    override suspend fun extendSession(sessionId: Int): Result<Hour> {
        return apiRequest("No se ha encontrado turno") { remote.extendSession(sessionId) }
    }

    override suspend fun endSession(sessionId: Int): Result<Boolean> {
        return apiRequest("No se ha encontrado turno") { remote.endSession(RequestDto(sessionId, Injection.actualTime)) }
    }

    private suspend fun<T: Any> apiRequest(error: String, doRequest: suspend () -> T): Result<T> {
        return withContext(coroutineContext) {
            try {
                val result = doRequest()
                Result.Success(result)
            } catch (ex: Exception) {
                Result.Error(IllegalArgumentException(error))
            }
        }
    }
}