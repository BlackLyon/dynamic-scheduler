package com.blacklyon.dynamicscheduler.infrastructure.repositories.inmemory

import com.blacklyon.dynamicscheduler.domain.Result
import com.blacklyon.dynamicscheduler.domain.user.UserRepository
import com.blacklyon.dynamicscheduler.domain.user.User

class InMemoryUserRepository: UserRepository {
    private val user = User(1, "Carlos", "Devilla",0)

    override suspend fun login(userName: String, password: String): Result<User> {
        return if (userName.isNotError()) Result.Success(user)
        else Result.Error(IllegalArgumentException("Credenciales incorrectas"))
    }

    override fun getUser(): User? = user

    private fun String.isNotError() = this != "Error"
}