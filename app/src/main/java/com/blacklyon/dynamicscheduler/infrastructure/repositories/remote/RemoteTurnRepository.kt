package com.blacklyon.dynamicscheduler.infrastructure.repositories.remote

import com.blacklyon.dynamicscheduler.domain.Result
import com.blacklyon.dynamicscheduler.domain.turn.Hour
import com.blacklyon.dynamicscheduler.domain.turn.Turn
import com.blacklyon.dynamicscheduler.domain.turn.TurnRepository
import com.blacklyon.dynamicscheduler.infrastructure.dto.RequestDto
import com.blacklyon.dynamicscheduler.infrastructure.retrofit.IPatientAPI
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.*
import kotlin.coroutines.CoroutineContext

class RemoteTurnRepository(
    private val remote: IPatientAPI,
    private val coroutineContext: CoroutineContext = Dispatchers.IO
): TurnRepository {

    override suspend fun getNextTurn(userId: Int): Result<Turn> {
        return apiRequest("No se ha encontrado turno") { remote.loadNextTurn(userId).toModel() }
    }

    override suspend fun cancelTurn(id: Int): Result<Hour> {
        return apiRequest("No se ha encontrado turno") { remote.cancelNextTurn(id) }
    }

    override suspend fun verifyAvailableTurn(userId: Int): Result<Hour> {
        return apiRequest("No hay turnos disponibles para adelantar") { remote.getAvailableTurnHour(userId) }
    }

    override suspend fun advanceTurn(userId: Int, hour: Hour): Result<Boolean> {
        return apiRequest("No se ha podido adelantar el turno") { remote.acceptTurn(RequestDto(userId, hour)) }
    }

    private suspend fun<T: Any> apiRequest(error: String, doRequest: suspend () -> T): Result<T> {
        return withContext(coroutineContext) {
            try {
                val result = doRequest()
                Result.Success(result)
            } catch (ex: Exception) {
                Result.Error(IllegalArgumentException(error))
            }
        }
    }
}


