package com.blacklyon.dynamicscheduler.infrastructure.dto

import com.blacklyon.dynamicscheduler.domain.turn.Hour
import com.google.gson.annotations.SerializedName

data class RequestDto (
    @SerializedName("id") val id: Int,
    @SerializedName("hour") val hour: Hour
)