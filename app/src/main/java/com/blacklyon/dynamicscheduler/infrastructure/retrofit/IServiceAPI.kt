package com.blacklyon.dynamicscheduler.infrastructure.retrofit

import com.blacklyon.dynamicscheduler.domain.user.Credentials
import com.blacklyon.dynamicscheduler.domain.user.User
import retrofit2.http.Body
import retrofit2.http.POST


interface IServiceAPI {

    @POST("login")
    suspend fun doLogin(@Body credentials: Credentials): User

}