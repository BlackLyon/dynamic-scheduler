package com.blacklyon.dynamicscheduler.infrastructure.retrofit

import com.blacklyon.dynamicscheduler.domain.turn.Hour
import com.blacklyon.dynamicscheduler.infrastructure.dto.RequestDto
import com.blacklyon.dynamicscheduler.infrastructure.dto.TurnDto
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface IPatientAPI {
    @GET("patient/turn/{userId}")
    suspend fun loadNextTurn(@Path("userId") userId: Int): TurnDto

    @GET("patient/turn/available/{userId}")
    suspend fun getAvailableTurnHour(@Path("userId") userId: Int): Hour

    @POST("patient/turn/cancel")
    suspend fun cancelNextTurn(@Body userId: Int): Hour

    @POST("patient/turn/accept")
    suspend fun acceptTurn(@Body body: RequestDto): Boolean
}