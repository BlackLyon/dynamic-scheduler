package com.blacklyon.dynamicscheduler.infrastructure.retrofit

import com.blacklyon.dynamicscheduler.domain.session.Session
import com.blacklyon.dynamicscheduler.domain.turn.Hour
import com.blacklyon.dynamicscheduler.domain.turn.Turn
import com.blacklyon.dynamicscheduler.infrastructure.dto.RequestDto
import retrofit2.http.Body
import retrofit2.http.POST

interface IDoctorAPI {

    @POST("doctor/turn/accept")
    suspend fun acceptNextTurn(@Body request: RequestDto): Session

    @POST("doctor/turn/emergency/accept")
    suspend fun acceptEmergency(@Body request: RequestDto): Session

    @POST("doctor/session/extend")
    suspend fun extendSession(@Body sessionId: Int): Hour

    @POST("doctor/session/end")
    suspend fun endSession(@Body request: RequestDto): Boolean

}