package com.blacklyon.dynamicscheduler.infrastructure.dto

import com.blacklyon.dynamicscheduler.domain.turn.Turn
import com.google.gson.annotations.SerializedName
import java.util.*

data class TurnDto(
    @SerializedName("id")
    var id: Int,
    @SerializedName("date")
    var date: Date,
    @SerializedName("hour")
    var hour: HourDto
){
    fun toModel() = Turn(
        id = id,
        dateTime = getModelDate(),
        doctorName = "Dr. Luisa Fereira",
        place = "Consultorio"
    )

    private fun getModelDate() = hour.toModel().getDate(date)

}