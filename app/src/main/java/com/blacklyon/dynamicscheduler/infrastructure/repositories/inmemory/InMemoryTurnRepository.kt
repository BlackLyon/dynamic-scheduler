package com.blacklyon.dynamicscheduler.infrastructure.repositories.inmemory

import com.blacklyon.dynamicscheduler.domain.Result
import com.blacklyon.dynamicscheduler.domain.turn.Hour
import com.blacklyon.dynamicscheduler.domain.turn.Turn
import com.blacklyon.dynamicscheduler.domain.turn.TurnRepository
import java.util.*

class InMemoryTurnRepository: TurnRepository {
    private var verified = false
    private var turn: Turn? =
        Turn(1, Date(), "Mario Rodriguez", "Consultorio escolar")

    override suspend fun getNextTurn(userId: Int): Result<Turn> {
        turn?.let { return Result.Success(it) }
        return Result.Error(IllegalArgumentException())
    }

    override suspend fun cancelTurn(id: Int) : Result<Hour> {
        turn?.let {
            return Result.Success(cancelledTurnHour(it.dateTime)).also { turn = null }
        }
        return Result.Error(IllegalArgumentException())
    }

    private fun cancelledTurnHour(turnDate: Date): Hour = Hour(Calendar.getInstance().apply { time = turnDate })

    override suspend fun verifyAvailableTurn(userId: Int): Result<Hour> {
        return if (verified) Result.Error(Exception("No hay turnos disponibles para adelantar"))
        else Result.Success(Hour(15, 10)).also { verified = true }
    }

    override suspend fun advanceTurn(userId: Int, hour: Hour): Result<Boolean> {
        return Result.Success(true)
    }
}
