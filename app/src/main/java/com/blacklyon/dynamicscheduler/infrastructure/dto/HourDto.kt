package com.blacklyon.dynamicscheduler.infrastructure.dto

import com.blacklyon.dynamicscheduler.domain.turn.Hour
import com.google.gson.annotations.SerializedName

data class HourDto(
    @SerializedName("hours")
    var hour: Int,
    @SerializedName("minutes")
    var minutes: Int
) {
    fun toModel() = Hour(
        hours = hour,
        minutes = minutes
    )
}