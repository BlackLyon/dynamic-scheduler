package com.blacklyon.dynamicscheduler.infrastructure.repositories.remote

import com.blacklyon.dynamicscheduler.domain.Result
import com.blacklyon.dynamicscheduler.domain.user.Credentials
import com.blacklyon.dynamicscheduler.domain.user.UserRepository
import com.blacklyon.dynamicscheduler.domain.user.User
import com.blacklyon.dynamicscheduler.infrastructure.retrofit.IServiceAPI
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlin.coroutines.CoroutineContext

class RemoteUserRepository(
    private val remote: IServiceAPI,
    private val coroutineContext: CoroutineContext = Dispatchers.IO
): UserRepository {

    private var user: User? = null

    override suspend fun login(userName: String, password: String): Result<User> {
        return getUser(userName, password)
    }

    private suspend fun getUser(userName: String, password: String): Result<User> {
        return withContext(coroutineContext) {
            try {
                remote.doLogin(Credentials(userName, password)).let {
                    setLoggedInUser(it)
                    Result.Success(it)
                }
            } catch (e: Exception) {
                Result.Error(IllegalArgumentException("Credenciales incorrectas"))
            }
        }
    }

    override fun getUser(): User? = user

    private fun setLoggedInUser(user: User) {
        this.user = user
    }
}