package com.blacklyon.dynamicscheduler.domain.user

import com.blacklyon.dynamicscheduler.domain.Result

interface UserRepository {
    suspend fun login(userName: String, password: String): Result<User>
    fun getUser(): User?
}