package com.blacklyon.dynamicscheduler.domain.turn

import com.blacklyon.dynamicscheduler.domain.Result

interface TurnRepository {
    suspend fun getNextTurn(userId: Int): Result<Turn>
    suspend fun cancelTurn(id: Int): Result<Hour>
    suspend fun verifyAvailableTurn(userId: Int): Result<Hour>
    suspend fun advanceTurn(userId: Int, hour: Hour): Result<Boolean>
}