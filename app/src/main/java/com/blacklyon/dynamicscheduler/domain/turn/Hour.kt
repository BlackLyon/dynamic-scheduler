package com.blacklyon.dynamicscheduler.domain.turn

import android.os.Parcel
import android.os.Parcelable
import java.util.*

data class Hour (
    var hours: Int,
    var minutes: Int
): Parcelable {

    constructor(calendar: Calendar): this(calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE))

    //region Parcelable implementation
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readInt()
    )

    fun getDate(base: Date) : Date {
        return Calendar.getInstance().apply {
            time = base
            set(Calendar.HOUR_OF_DAY, hours)
            set(Calendar.MINUTE, minutes)
        }.time
    }

    override fun toString(): String {
        return "$hours:$minutes"
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(hours)
        parcel.writeInt(minutes)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Hour> {
        override fun createFromParcel(parcel: Parcel): Hour {
            return Hour(parcel)
        }

        override fun newArray(size: Int): Array<Hour?> {
            return arrayOfNulls(size)
        }
    }
    //endregion
}