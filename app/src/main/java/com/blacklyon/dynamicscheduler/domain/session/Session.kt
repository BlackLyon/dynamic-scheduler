package com.blacklyon.dynamicscheduler.domain.session

import android.os.Parcel
import android.os.Parcelable
import com.blacklyon.dynamicscheduler.domain.turn.Hour

data class Session (
    val id: Int,
    val initTime: Hour,
    val endTime: Hour,
    val patientName: String?
): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readParcelable(Hour::class.java.classLoader)!!,
        parcel.readParcelable(Hour::class.java.classLoader)!!,
        parcel.readString()!!
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeParcelable(initTime, flags)
        parcel.writeParcelable(endTime, flags)
        parcel.writeString(patientName)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Session> {
        override fun createFromParcel(parcel: Parcel): Session {
            return Session(parcel)
        }

        override fun newArray(size: Int): Array<Session?> {
            return arrayOfNulls(size)
        }
    }

}