package com.blacklyon.dynamicscheduler.domain.user

data class User(
    val id: Int,
    val name: String,
    val lastName: String,
    val type: Int
)