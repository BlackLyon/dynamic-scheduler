package com.blacklyon.dynamicscheduler.domain.turn

import android.os.Parcel
import android.os.Parcelable
import java.text.SimpleDateFormat
import java.util.*

data class Turn (
    val id: Int,
    val dateTime: Date,
    val doctorName: String,
    val place: String
): Parcelable {

    val time: String
        get() = dateTime.format("HH:mm a")

    val date: String
        get() = dateTime.format("dd/MM/yyyy")

    private fun Date.format(pattern: String) : String {
        return SimpleDateFormat(pattern, Locale.getDefault()).format(this)
    }

    //region Parcelable Implementation
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        Date(parcel.readLong()),
        parcel.readString() ?: "",
        parcel.readString() ?: ""
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeLong(dateTime.time)
        parcel.writeString(doctorName)
        parcel.writeString(place)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Turn> {
        override fun createFromParcel(parcel: Parcel): Turn {
            return Turn(parcel)
        }

        override fun newArray(size: Int): Array<Turn?> {
            return arrayOfNulls(size)
        }
    }
    //endregion
}