package com.blacklyon.dynamicscheduler.domain.user

import com.google.gson.annotations.SerializedName

data class Credentials(
    @SerializedName("DNI") val dni: String,
    @SerializedName("Password") val password: String
)