package com.blacklyon.dynamicscheduler.domain.session

import com.blacklyon.dynamicscheduler.domain.Result
import com.blacklyon.dynamicscheduler.domain.turn.Hour

interface SessionRepository {
    suspend fun acceptTurn(doctorId: Int): Result<Session>
    suspend fun extendSession(sessionId: Int): Result<Hour>
    suspend fun endSession(sessionId: Int): Result<Boolean>
    suspend fun acceptEmergency(doctorId: Int): Result<Session>
}