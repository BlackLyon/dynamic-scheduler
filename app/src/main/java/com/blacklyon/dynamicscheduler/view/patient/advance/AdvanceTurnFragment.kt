package com.blacklyon.dynamicscheduler.view.patient.advance

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.addCallback
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.blacklyon.dynamicscheduler.R
import com.blacklyon.dynamicscheduler.domain.turn.Hour
import com.blacklyon.dynamicscheduler.domain.turn.Turn
import com.blacklyon.dynamicscheduler.injection.Injection
import com.blacklyon.dynamicscheduler.injection.ViewModelFactory
import kotlinx.android.synthetic.main.fragment_advance_turn.*

class AdvanceTurnFragment : Fragment() {

    private val viewModel: AdvanceTurnViewModel by viewModels { ViewModelFactory { Injection.advanceTurnViewModel() } }

    init {
        lifecycleScope.launchWhenCreated {
            requireActivity().onBackPressedDispatcher.addCallback(this@AdvanceTurnFragment) { }
        }
        lifecycleScope.launchWhenStarted {
            populateArguments()
            setupViews()
        }
    }

    private fun setupViews() {
        btn_accept.setOnClickListener {
            viewModel.advanceTurn.observe(viewLifecycleOwner, Observer { result ->
                when(result) {
                    true -> Toast.makeText(context, R.string.accepted_turn, Toast.LENGTH_SHORT).show()
                    false -> Toast.makeText(context, R.string.error_accepting_turn, Toast.LENGTH_SHORT).show()
                }
                findNavController().navigate(R.id.action_advanceTurnFragment_to_MainFragment)
            })
        }
        btn_reject.setOnClickListener {
            findNavController().navigate(R.id.action_advanceTurnFragment_to_MainFragment)
        }
    }

    private fun populateArguments() {
        arguments?.getParcelable<Hour>("hour")?.let {
            viewModel.hour = it
            val time = "$it?"
            tv_advance_turn_title.text = time
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_advance_turn, container, false)
    }

}