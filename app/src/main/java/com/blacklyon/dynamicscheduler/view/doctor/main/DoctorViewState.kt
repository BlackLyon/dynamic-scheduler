package com.blacklyon.dynamicscheduler.view.doctor.main

import com.blacklyon.dynamicscheduler.domain.session.Session

data class DoctorViewState(
    val turnAccepted: Session? = null,
    val emergencyAccepted: Session? = null,
    val errorAcceptingTurn: Int? = null
)