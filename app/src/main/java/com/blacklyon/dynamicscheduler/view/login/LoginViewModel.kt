package com.blacklyon.dynamicscheduler.view.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.blacklyon.dynamicscheduler.R
import com.blacklyon.dynamicscheduler.domain.Result
import com.blacklyon.dynamicscheduler.domain.user.UserRepository
import com.blacklyon.dynamicscheduler.domain.user.User
import com.blacklyon.dynamicscheduler.view.login.UserType.*
import kotlinx.coroutines.launch


class LoginViewModel(private val userRepository: UserRepository) : ViewModel() {

    private val _loginForm = MutableLiveData<LoginFormState>()
    val loginFormState: LiveData<LoginFormState> = _loginForm

    private val _loginResult = MutableLiveData<LoginResult>()
    val loginResult: LiveData<LoginResult> = _loginResult

    fun login(username: String, password: String) {
        viewModelScope.launch {
            _loginResult.value = userRepository.login(username, password).getValue()
        }
    }

    fun loginDataChanged(username: String, password: String) {
        _loginForm.value = loginFormState(username, password)
    }

    private fun Result<User>.getValue(): LoginResult? {
        return when (this) {
            is Result.Success -> LoginResult(
                success = UserView(
                    displayName = "${data.name} ${data.lastName}",
                    type = data.userType()
                )
            )
            else -> LoginResult(
                error = R.string.login_failed
            )
        }
    }

    private fun loginFormState(username: String, password: String): LoginFormState {
        return if (!isUserNameValid(username)) LoginFormState(
            usernameError = R.string.invalid_username
        )
        else if (!isPasswordValid(password)) LoginFormState(
            passwordError = R.string.invalid_password
        )
        else LoginFormState(
            isDataValid = true
        )
    }
    private fun User.userType(): UserType {
        return when (this.type) {
            Patient.value -> Patient
            Doctor.value -> Doctor
            else -> Patient
        }
    }
    private fun isUserNameValid(username: String) = username.isNotBlank()
    private fun isPasswordValid(password: String) = password.length > 5
}