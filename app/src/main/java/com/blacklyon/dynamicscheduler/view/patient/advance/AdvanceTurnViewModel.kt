package com.blacklyon.dynamicscheduler.view.patient.advance

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.blacklyon.dynamicscheduler.domain.Result
import com.blacklyon.dynamicscheduler.domain.turn.Hour
import com.blacklyon.dynamicscheduler.domain.turn.Turn
import com.blacklyon.dynamicscheduler.domain.turn.TurnRepository
import com.blacklyon.dynamicscheduler.domain.user.UserRepository
import java.util.*

class AdvanceTurnViewModel(private val userRepository: UserRepository, private val turnRepository: TurnRepository) : ViewModel() {
    lateinit var hour: Hour

    val advanceTurn = liveData {
        userRepository.getUser()?.let {
            val result = turnRepository.advanceTurn(it.id, hour).value()
            emit(result)
        }
    }

    private fun Result<Boolean>.value(): Boolean {
        return this is Result.Success
    }
}