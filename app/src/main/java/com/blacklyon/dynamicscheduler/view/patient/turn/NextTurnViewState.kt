package com.blacklyon.dynamicscheduler.view.patient.turn

data class NextTurnViewState (
    val turnCancelled: Int? = null,
    val userError: Int? = null,
    val nextTurnError: Int? = null,
    val cancelTurnError: Int? = null
)