package com.blacklyon.dynamicscheduler.view.login

enum class UserType(val value: Int) {
    Patient(0),
    Doctor(1)
}

data class UserView(
    val displayName: String,
    val type: UserType
)