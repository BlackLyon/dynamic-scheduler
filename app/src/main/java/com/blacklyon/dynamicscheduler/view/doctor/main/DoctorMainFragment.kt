package com.blacklyon.dynamicscheduler.view.doctor.main

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.blacklyon.dynamicscheduler.R
import com.blacklyon.dynamicscheduler.domain.session.Session
import com.blacklyon.dynamicscheduler.injection.Injection
import com.blacklyon.dynamicscheduler.injection.ViewModelFactory
import kotlinx.android.synthetic.main.doctor_main_fragment.*

class DoctorMainFragment : Fragment() {

    private val viewModel: DoctorViewModel by viewModels { ViewModelFactory { Injection.doctorViewModel() } }

    init {
        lifecycleScope.launchWhenStarted {
            setupCurrentUser()
            setupViews()
            observeViewState()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.doctor_main_fragment, container, false)
    }

    private fun observeViewState() {
        viewModel.viewState.observe(viewLifecycleOwner, Observer {
            val state = it ?: return@Observer

            state.turnAccepted?.let { turn ->
                navigateToSessionView(turn)
            }

            state.emergencyAccepted?.let { turn ->
                navigateToSessionView(turn, true)
            }

            state.errorAcceptingTurn?.let { error ->
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun setupViews() {
        btn_receive_turn.setOnClickListener {
            //acceptTurn()
            viewModel.acceptNextTurn()
        }
        btn_receive_no_scheduled_turn.setOnClickListener {
            // acceptEmergency()
            viewModel.acceptEmergency()
        }
    }

    private fun setupCurrentUser() {
        viewModel.currentUser.observe(viewLifecycleOwner, Observer {
            val userName = "Dr(a). ${it.name} ${it.lastName}"
            tv_doctor_name.text = userName
        })
    }

    //TODO Seguir revisando
    private val viewStateObserver = Observer<DoctorViewState> {
        val state = it ?: return@Observer

        state.turnAccepted?.let { turn ->
            navigateToSessionView(turn)
        }

        state.emergencyAccepted?.let { turn ->
            navigateToSessionView(turn, true)
        }

        state.errorAcceptingTurn?.let { error ->
            Toast.makeText(context, error, Toast.LENGTH_SHORT).show()
        }
    }

    private fun acceptTurn() {
        viewModel.acceptTurn.observe(viewLifecycleOwner, viewStateObserver)
    }

    private fun acceptEmergency(){
        viewModel.acceptEmergency.observe(viewLifecycleOwner, viewStateObserver)
    }

    private fun navigateToSessionView(session: Session, isEmergency: Boolean = false) {
        val bundle = Bundle().apply {
            putParcelable("session", session)
            putBoolean("isEmergency", isEmergency)
        }
        findNavController().navigate(R.id.action_doctorMain_to_session, bundle)
    }
}