package com.blacklyon.dynamicscheduler.view.login

/**
 * Authentication result : success (user details) or error message.
 */
data class LoginResult(
    val success: UserView? = null,
    val error: Int? = null
)