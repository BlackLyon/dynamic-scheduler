package com.blacklyon.dynamicscheduler.view.patient.advance

data class AdvanceTurnViewState(
    val success: Int? = null,
    val error: Int? = null
)