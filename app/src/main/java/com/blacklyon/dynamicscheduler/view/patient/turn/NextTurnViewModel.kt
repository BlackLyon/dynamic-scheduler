package com.blacklyon.dynamicscheduler.view.patient.turn

import androidx.lifecycle.*
import com.blacklyon.dynamicscheduler.domain.Result
import com.blacklyon.dynamicscheduler.domain.turn.Turn
import com.blacklyon.dynamicscheduler.domain.turn.TurnRepository
import com.blacklyon.dynamicscheduler.domain.user.UserRepository
import kotlinx.coroutines.launch
import com.blacklyon.dynamicscheduler.R
import com.blacklyon.dynamicscheduler.domain.turn.Hour
import com.blacklyon.dynamicscheduler.domain.user.User


class NextTurnViewModel(
    private val userRepository: UserRepository,
    private val repository: TurnRepository
): ViewModel() {

    private var _currentUser: User? = null
    private val _nextTurn = MutableLiveData<NextTurnViewState>()
    val nextTurnState: LiveData<NextTurnViewState> = _nextTurn

    val turn: LiveData<Turn?> = liveData {
        _currentUser = userRepository.getUser()
        _currentUser?.let { user -> emit(
            repository.getNextTurn(user.id).value()
        )} ?: run { emitUserError() }
    }

    fun cancelNextTurn() {
        turn.value?.let {
            viewModelScope.launch { cancelTurn() }
        } ?: run { emitTurnError() }
    }

    private suspend fun cancelTurn() {
        _nextTurn.value = _currentUser?.run {
            repository.cancelTurn(id).value()
        }
    }

    private fun Result<Hour>.value(): NextTurnViewState {
        return when(this) {
            is Result.Success -> NextTurnViewState(turnCancelled = R.string.turn_cancelled)
            is Result.Error -> NextTurnViewState(cancelTurnError = R.string.error_cancelling_turn)
        }
    }

    private fun Result<Turn>.value(): Turn? {
        return when (this){
            is Result.Success -> data
            else -> emitTurnError().run { return null }
        }
    }

    private fun emitUserError() {
        _nextTurn.value = NextTurnViewState(userError = R.string.not_user_logged_in)
    }

    private fun emitTurnError() {
        _nextTurn.value = NextTurnViewState(nextTurnError = R.string.not_next_turn)
    }
}

