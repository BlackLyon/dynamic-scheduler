package com.blacklyon.dynamicscheduler.view.patient.turn

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.blacklyon.dynamicscheduler.R
import com.blacklyon.dynamicscheduler.injection.Injection
import com.blacklyon.dynamicscheduler.injection.ViewModelFactory
import kotlinx.android.synthetic.main.fragment_next_turn.*

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class NextTurnFragment : Fragment() {
    private val viewModel: NextTurnViewModel by viewModels { ViewModelFactory { Injection.nextTurnViewModel() } }
    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_next_turn, container, false)
    }

    init {
        lifecycleScope.launchWhenStarted {
            observeNextTurn()
            observeViewState()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btn_ok.setOnClickListener {
            findNavController().navigate(R.id.action_NextTurnFragment_to_MainFragment)
        }

        btn_cancel_next_turn.setOnClickListener {
            viewModel.cancelNextTurn()
        }
    }

    private fun observeViewState() {
        viewModel.nextTurnState.observe(viewLifecycleOwner, Observer {
            val state = it ?: return@Observer

            state.turnCancelled?.let { value ->
                Toast.makeText(this.context, value, Toast.LENGTH_SHORT).show()
                btn_cancel_next_turn.visibility = View.INVISIBLE
                tv_turn_title.text = getString(value)
            }

            state.nextTurnError?.let { error ->
                tv_turn_title.text = getString(error)
                btn_cancel_next_turn.visibility = View.INVISIBLE
            }
        })
    }

    private fun observeNextTurn() {
        viewModel.turn.observe(viewLifecycleOwner, Observer { turn ->
            turn?.let {
                tv_turn_date.text = it.date
                tv_turn_time.text = it.time
                tv_turn_doctor.text = it.doctorName
                tv_turn_place.text = it.place
            }
        })
    }
}