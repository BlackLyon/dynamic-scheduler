package com.blacklyon.dynamicscheduler.view.patient.main

import com.blacklyon.dynamicscheduler.domain.turn.Hour

data class PatientMenuViewState (
    val availableTurn: Hour? = null,
    val userError: Int? = null
)