package com.blacklyon.dynamicscheduler.view.patient.main

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.blacklyon.dynamicscheduler.R
import com.blacklyon.dynamicscheduler.injection.Injection
import com.blacklyon.dynamicscheduler.injection.ViewModelFactory
import kotlinx.android.synthetic.main.fragment_main.*

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class PatientMainFragment : Fragment() {
    private val viewModel: MenuViewModel by viewModels { ViewModelFactory { Injection.menuViewModel() } }
    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    init {
        lifecycleScope.launchWhenStarted {
            Log.d("COROUTINE", "PatientMainFragment launchWhenStated")
            observeViewState()
            setClickListeners()
            verifyAvailableTurn()
        }
    }

    private fun observeViewState() {
        viewModel.patientMenuState.observe(viewLifecycleOwner, Observer {
            val state = it ?: return@Observer

            state.availableTurn?.let { hour ->
                val bundle = Bundle().apply { putParcelable("hour", hour) }
                findNavController().navigate(R.id.action_MainFragment_to_advanceTurnFragment, bundle)
            }

            state.userError?.let { message ->
                Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
            }
        })

        viewModel.loggedInUser.observe(viewLifecycleOwner, Observer {
            val userName = "${it.name} ${it.lastName}"
            tv_patient_name.text = userName
        })
    }

    private fun verifyAvailableTurn() {
//        viewModel.verifyTurnAvailable()
        viewModel.availableTurn.observe(viewLifecycleOwner, Observer { hour ->
            val bundle = Bundle().apply { putParcelable("hour", hour) }
            findNavController().navigate(R.id.action_MainFragment_to_advanceTurnFragment, bundle)
        })
    }

    private fun setClickListeners() {
        btn_next_turn.setOnClickListener {
            findNavController().navigate(R.id.action_MainFragment_to_NextTurnFragment)
        }
    }
}