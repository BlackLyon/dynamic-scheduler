package com.blacklyon.dynamicscheduler.view.doctor.main

import androidx.lifecycle.*
import com.blacklyon.dynamicscheduler.R
import com.blacklyon.dynamicscheduler.domain.Result
import com.blacklyon.dynamicscheduler.domain.session.Session
import com.blacklyon.dynamicscheduler.domain.session.SessionRepository
import com.blacklyon.dynamicscheduler.domain.user.User
import com.blacklyon.dynamicscheduler.domain.user.UserRepository
import kotlinx.coroutines.launch

class DoctorViewModel(private val userRepository: UserRepository, private val sessionRepository: SessionRepository) : ViewModel() {

    private var _currentUser: User? = null

    private val _viewState = MutableLiveData<DoctorViewState>()
    val viewState: LiveData<DoctorViewState> = _viewState

    val currentUser = liveData {
        userRepository.getUser()?.let {
            _currentUser = it
            emit(it)
        }
    }

    val acceptTurn: LiveData<DoctorViewState> = liveData {
        _currentUser?.run {
            emit(sessionRepository.acceptTurn(id).value())
        }
    }

    val acceptEmergency: LiveData<DoctorViewState> = liveData {
        _currentUser?.run {
            emit(sessionRepository.acceptEmergency(id).value())
        }
    }

    fun acceptNextTurn() {
        viewModelScope.launch { acceptTurn() }
    }

    fun acceptEmergency() {
        viewModelScope.launch { acceptEmergencyTurn() }
    }

    private suspend fun acceptTurn() {
        _currentUser?.run { _viewState.postValue((sessionRepository.acceptTurn(id).value())) }
    }

    private suspend fun acceptEmergencyTurn() {
        _currentUser?.run { _viewState.value = sessionRepository.acceptEmergency(id).value() }
    }

    private fun Result<Session>.value(): DoctorViewState {
        return when (this) {
            is Result.Success -> DoctorViewState(turnAccepted = this.data)
            else -> DoctorViewState(errorAcceptingTurn = R.string.error_accepting_next_turn)
        }
    }
}
