package com.blacklyon.dynamicscheduler.view.patient.main

import androidx.lifecycle.*
import com.blacklyon.dynamicscheduler.R
import com.blacklyon.dynamicscheduler.domain.Result
import com.blacklyon.dynamicscheduler.domain.turn.Hour
import com.blacklyon.dynamicscheduler.domain.turn.TurnRepository
import com.blacklyon.dynamicscheduler.domain.user.User
import com.blacklyon.dynamicscheduler.domain.user.UserRepository
import kotlinx.coroutines.launch

class MenuViewModel(private val userRepository: UserRepository, private val turnRepository: TurnRepository) : ViewModel() {

    private val _patientMenuState = MutableLiveData<PatientMenuViewState>()
    val patientMenuState: LiveData<PatientMenuViewState> = _patientMenuState
    val loggedInUser = MutableLiveData<User>()


    val availableTurn: LiveData<Hour> = liveData {
        userRepository.getUser()?.let { user ->
            loggedInUser.value = user
            turnRepository.verifyAvailableTurn(user.id).value()?.let { emit(it) }
            return@let
        } ?: run {  emitUserError() }
    }

    fun verifyTurnAvailable() {
        viewModelScope.launch {
            userRepository.getUser()?.let { user ->
                loggedInUser.value = user
                turnRepository.verifyAvailableTurn(user.id).value()?.let {
                    _patientMenuState.value = PatientMenuViewState(availableTurn = it)
                }} ?:
            run {  emitUserError() }
        }
    }

    private fun Result<Hour>.value(): Hour? {
        return when (this){
            is Result.Success -> data
            else -> null
        }
    }

    private fun emitUserError() {
        _patientMenuState.value = PatientMenuViewState(userError = R.string.not_user_logged_in)
    }
}