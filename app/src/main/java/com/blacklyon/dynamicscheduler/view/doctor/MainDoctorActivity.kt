package com.blacklyon.dynamicscheduler.view.doctor

import android.app.TimePickerDialog
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.blacklyon.dynamicscheduler.R
import com.blacklyon.dynamicscheduler.injection.Injection

class MainDoctorActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_doctor_main)
        setSupportActionBar(findViewById(R.id.toolbar))
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_time -> showTimePicker()
            R.id.action_logout -> finish()
        }
        return super.onOptionsItemSelected(item)
    }
    private fun showTimePicker() {
        TimePickerDialog(
            this,
            { _, hourOfDay, minute ->
                Injection.actualTime.hours = hourOfDay
                Injection.actualTime.minutes = minute
            },
            Injection.actualTime.hours,
            Injection.actualTime.minutes,
            true
        ).show()
    }
}