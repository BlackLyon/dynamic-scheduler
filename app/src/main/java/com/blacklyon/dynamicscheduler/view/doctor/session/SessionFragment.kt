package com.blacklyon.dynamicscheduler.view.doctor.session

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.addCallback
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.blacklyon.dynamicscheduler.R
import com.blacklyon.dynamicscheduler.domain.session.Session
import com.blacklyon.dynamicscheduler.domain.turn.Hour
import com.blacklyon.dynamicscheduler.injection.Injection
import com.blacklyon.dynamicscheduler.injection.ViewModelFactory
import kotlinx.android.synthetic.main.session_fragment.*

class SessionFragment : Fragment() {

    private val viewModel: SessionViewModel
            by viewModels { ViewModelFactory { Injection.sessionViewModel() } }

    init {
        lifecycleScope.launchWhenStarted {
            observeViewState()
            setupArguments()
            setupViews()
        }

        lifecycleScope.launchWhenCreated {
            requireActivity().onBackPressedDispatcher.addCallback(this@SessionFragment) { }
        }
    }

    private fun setupViews() {
        btn_end_turn.setOnClickListener { viewModel.endSession() }
        btn_extend_turn.setOnClickListener { viewModel.extendSession() }
    }

    private fun observeViewState() {
        viewModel.viewState.observe(viewLifecycleOwner, Observer { state ->
            state.verifyCurrentSession()
            state.verifyExtendedHour()
            state.verifySessionEnded()
        })
    }

    private fun SessionViewState.verifySessionEnded() {
        turnEnded?.let {
            showToast("El turno fue cerrado correctamente")
            findNavController().navigate(R.id.action_sessionFragment_to_doctorMainFragment)
            //findNavController().popBackStack()
        }
    }

    private fun SessionViewState.verifyExtendedHour() {
        extendedHour?.let { hour ->
            showToast("El turno fue extendido correctamente")
            populateHours(null, hour)
        }
    }

    private fun SessionViewState.verifyCurrentSession() {
        currentTurn?.let { session -> populateSession(session) }
    }

    private fun populateSession(session: Session) {
        tv_patient_name.text = session.patientName
        populateHours(session.initTime, session.endTime)
    }

    private fun populateHours(init: Hour?, end: Hour?) {
        init?.run { tv_session_init.text = "${hours}:${minutes}" }
        end?.run { tv_session_end.text = "${hours}:${minutes}" }
    }

    private fun setupArguments() {
        arguments?.getParcelable<Session>("session")?.let {
            viewModel.setCurrentSession(it)
        }
    }

    private fun showToast(msg: String) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.session_fragment, container, false)
    }

}