package com.blacklyon.dynamicscheduler.view.doctor.session

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.blacklyon.dynamicscheduler.domain.Result
import com.blacklyon.dynamicscheduler.domain.session.Session
import com.blacklyon.dynamicscheduler.domain.session.SessionRepository
import com.blacklyon.dynamicscheduler.domain.turn.Hour
import kotlinx.coroutines.launch

class SessionViewModel(private val sessionRepository: SessionRepository) : ViewModel() {
    private var currentSession: Session? = null

    private var _viewState = MutableLiveData<SessionViewState>()
    var viewState: LiveData<SessionViewState> = _viewState

    fun setCurrentSession(session: Session) {
        currentSession = session
        _viewState.value = SessionViewState(currentTurn = session)
    }

    fun extendSession() {
        viewModelScope.launch { extendCurrentSession() }
    }

    fun endSession() {
        viewModelScope.launch { endCurrentSession() }
    }

    private suspend fun extendCurrentSession() {
        currentSession?.let {
            val extendedHour = sessionRepository.extendSession(it.id).value()
            extendedHour.also { sessionViewState ->
                currentSession = sessionViewState.extendedHour?.run { it.copy(endTime = this) }
                _viewState.value = sessionViewState
            }
        }
    }

    private suspend fun endCurrentSession() {
        currentSession?.let {
            val result = sessionRepository.endSession(it.id)
            _viewState.value = result.valueBool()
        }
    }
}

private fun Result<Hour>.value(): SessionViewState {
    return when (this) {
        is Result.Success -> SessionViewState(extendedHour = this.data)
        else -> throw NotImplementedError("Not implemented yet")
    }
}

private fun Result<Boolean>.valueBool(): SessionViewState {
    return when (this) {
        is Result.Success ->  SessionViewState(turnEnded = this.data)
        else -> throw NotImplementedError("Not implemented yet")
    }
}

data class SessionViewState(
    var currentTurn: Session? = null,
    var extendedHour: Hour? = null,
    var turnEnded: Boolean? = null
)