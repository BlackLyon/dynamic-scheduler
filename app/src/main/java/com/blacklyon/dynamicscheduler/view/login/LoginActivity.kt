package com.blacklyon.dynamicscheduler.view.login

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.Toast
import androidx.activity.viewModels
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.blacklyon.dynamicscheduler.R
import com.blacklyon.dynamicscheduler.injection.Injection
import com.blacklyon.dynamicscheduler.injection.ViewModelFactory
import com.blacklyon.dynamicscheduler.view.patient.MainActivity
import com.blacklyon.dynamicscheduler.view.doctor.MainDoctorActivity
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    private val loginViewModel: LoginViewModel by viewModels { ViewModelFactory { Injection.loginViewModel() } }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        observeFormState()
        observeLoginResult()

        setupLoginForm()
        setLoginClickListener()
    }

    private fun observeFormState() {
        loginViewModel.loginFormState.observe(this@LoginActivity, Observer {
            val loginState = it ?: return@Observer
            login.isEnabled = loginState.isDataValid

            loginState.usernameError?.let { error ->
                username.error = getString(error)
            }
            loginState.passwordError?.let { error ->
                password.error = getString(error)
            }
        })
    }

    private fun observeLoginResult() {
        loginViewModel.loginResult.observe(this@LoginActivity, Observer {
            val loginResult = it ?: return@Observer

            loading.visibility = View.GONE
            loginResult.error?.let { error ->
                showLoginFailed(error)
            }
            loginResult.success?.let { success ->
                updateUiWithUser(success)
            }
        })
    }

    private fun setupLoginForm() {
        username.afterTextChanged { loginDataChanged() }

        password.apply {
            afterTextChanged { loginDataChanged() }

            setOnEditorActionListener { _, actionId, _ ->
                when (actionId) { EditorInfo.IME_ACTION_DONE -> doLogin() }
                false
            }
        }
    }

    private fun updateUiWithUser(model: UserView) {
        showWelcomeMsg(model)
        when(model.type) {
            UserType.Patient -> startPatientView()
            UserType.Doctor -> startDoctorView()
        }
    }

    private fun startPatientView() {
        startActivity(Intent(this@LoginActivity, MainActivity::class.java))
    }

    private fun startDoctorView() {
        startActivity(Intent(this@LoginActivity, MainDoctorActivity::class.java))
    }

    private fun setLoginClickListener() {
        login.setOnClickListener { doLogin() }
        btn_credentials.setOnClickListener {
            if (username.text.toString() == "11111111") {
                username.setText("11111123")
                password.setText("Medico23!")
            } else {
                username.setText("11111111")
                password.setText("Paciente1!")
            }
        }
    }

    private fun loginDataChanged() {
        loginViewModel.loginDataChanged(
            username.text.toString(),
            password.text.toString()
        )
    }

    private fun doLogin() {
        loading.visibility = View.VISIBLE
        loginViewModel.login(
            username.text.toString(),
            password.text.toString()
        )
    }

    private fun showLoginFailed(@StringRes errorString: Int) {
        Toast.makeText(applicationContext, errorString, Toast.LENGTH_SHORT).show()
    }

    private fun showWelcomeMsg(model: UserView) {
        val welcome = getString(R.string.welcome)
        val displayName = model.displayName
        Toast.makeText(applicationContext, "$welcome $displayName", Toast.LENGTH_SHORT).show()
    }
}

fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(editable: Editable?) {
            afterTextChanged.invoke(editable.toString())
        }

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
    })
}